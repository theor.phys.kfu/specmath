#ifndef INTERPOLATOR_H
#define INTERPOLATOR_H

#include <cmath>
#include <limits>
#include <algorithm>
#include <iterator>

#include <specmath/common_functions.h>


namespace specmath {

namespace detail {

template <class Real>
Real deriv(const Real & x0, const Real & x1, const Real & x2,
           const Real & f0, const Real & f1, const Real & f2)
{
    Real h1 = x1 - x0;
    Real h2 = x2 - x1;

    return ((f1-f0)*h2/h1 + (f2-f1)*h1/h2)/(h1 + h2);
}

template <class Real>
Real b3_spline(const Real & x, const Real & x0, const Real& x1,
                               const Real & f0, const Real & f1,
                               const Real & prime_f0, const Real & prime_f1)
{
    Real h = x1 - x0;
    Real dx0 = x - x0;
    Real dx1 = x - x1;
    Real df_div_h = (f1 - f0)/h;

    Real c = std::isnan(prime_f0) ? static_cast<Real>(0) : (prime_f0 - df_div_h)/h;
    Real d = std::isnan(prime_f1) ? static_cast<Real>(0) : (prime_f1 - df_div_h)/h;

    return ( dx0*(f1 + c*sqr(dx1)) + dx1*(-f0 + d*sqr(dx0)) )/h;
}

} /* namespace detail */


template <class Real, class BidiIterator_y, class BidiIterator_x>
Real interpolate(const Real & x, BidiIterator_y start_y, BidiIterator_y end_y, BidiIterator_x start_x,
                 const Real & left_endpoint_derivative = std::numeric_limits<Real>::quiet_NaN(),
                 const Real & right_endpoint_derivative = std::numeric_limits<Real>::quiet_NaN())
{
    auto size = std::distance(start_y, end_y);
    auto end_x = std::next(start_x, size);

    auto xi = std::lower_bound(start_x, end_x, x);

    if (xi == end_x)
        return *(--end_y);

    auto pos = std::distance(start_x, xi);
    auto fi = std::next(start_y, pos);

    if (0 == pos)
        return *start_y;

    Real dfl = (1 == pos) ? left_endpoint_derivative
                          : detail::deriv(*std::prev(xi, 2), *std::prev(xi), *xi, *std::prev(fi, 2), *std::prev(fi), *fi);

    Real dfr = (size-1 == pos) ? right_endpoint_derivative
                               : detail::deriv(*std::prev(xi), *xi, *std::next(xi), *std::prev(fi), *fi, *std::next(fi));

    return detail::b3_spline(x, *std::prev(xi), *xi, *std::prev(fi), *fi, dfl, dfr);
}

} /* namespace specmath */

#endif // INTERPOLATOR_H
