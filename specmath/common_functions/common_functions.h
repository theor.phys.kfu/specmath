#ifndef COMMON_FUNCTIONS_H
#define COMMON_FUNCTIONS_H

#include <limits>
#include <cmath>

namespace specmath {

template <class T>
constexpr int sign(const T& x) { return x >= T{0} ? 1 : -1; }

inline constexpr bool is_odd(int i) { return i & 1; }

inline constexpr bool is_even(int i) { return !is_odd(i); }

inline constexpr bool delta(int i, int j) { return (i == j); }

template <class T>
constexpr T sqr(const T& x)
{
    return x*x;
}

template <class T>
constexpr T cube(const T& x)
{
    return x*x*x;
}

template <class T>
constexpr T quad(const T& x)
{
    return x*x*x*x;
}

template <class T>
constexpr T theta(const T& x)
{
    return (x > T{0}) ? T{1} : T{0};
}

/*
 * th_div_x(x) = tanh(x)/x
*/
template <class T>
inline double th_div_x(const T& x)
{
    static constexpr T eps = 100*std::numeric_limits<T>::epsilon();

    return (fabs(x) < eps) ? T{1} : tanh(x)/x;
}

} /* namespace specmath */

#endif // COMMON_FUNCTIONS_H
