#ifndef SPECMATH_WANDA_H
#define SPECMATH_WANDA_H

#include <specmath/wanda/util.h>
#include <specmath/wanda/training_dataset.h>
#include <specmath/wanda/kd_tree.h>
#include <specmath/wanda/wanda.h>


#endif // SPECMATH_WANDA_H
