#ifndef GRID2D_H
#define GRID2D_H

#include <type_traits>
#include <vector>
#include <iostream>
#include <functional>
#include <cstddef>
#include <algorithm>

#include <specmath/thread_pool.h>

#include "scale.h"
#include "point2d.h"

namespace specmath {

template <class T, class ScaleX = linear_scale, class ScaleY = linear_scale>
class grid2d_iterator
{
public:
    typedef T real_type;
    typedef point2d<T>  value_type;
    typedef ptrdiff_t difference_type;
    typedef std::random_access_iterator_tag  iterator_category;
    typedef const point2d<T>* pointer;
    typedef const point2d<T>& reference;
    typedef size_t size_type;
    typedef ScaleX xscale_type;
    typedef ScaleY yscale_type;

    grid2d_iterator() : m_pos(0), m_xmin(0), m_xmax(0), m_ymin(0), m_ymax(0), m_size_x(2), m_size_y(2)  {}

    grid2d_iterator(size_type pos,
                    const real_type& xmin, const real_type& xmax,
                    const real_type& ymin, const real_type& ymax,
                    size_type Nx, size_type Ny)

        : m_pos(pos), m_xmin(xmin), m_xmax(xmax), m_ymin(ymin), m_ymax(ymax), m_size_x(Nx), m_size_y(Ny) {}

    grid2d_iterator(const grid2d_iterator&) = default;

    value_type operator*() const
    {
        size_type j = m_pos % m_size_y;
        size_type i = (m_pos - j)/m_size_y;

        return value_type(xscale_type::get(i, m_xmin, m_xmax, m_size_x), yscale_type::get(j, m_ymin, m_ymax, m_size_y));
    }

    grid2d_iterator& operator=(const grid2d_iterator&) = default;

    grid2d_iterator& operator++()
    {
        ++m_pos;
        return *this;
    }

    grid2d_iterator operator++(int)
    {
        grid2d_iterator tmp(*this);
        ++m_pos;
        return tmp;
    }

    grid2d_iterator& operator--()
    {
        --m_pos;
        return *this;
    }

    grid2d_iterator operator--(int)
    {
        grid2d_iterator tmp(*this);
        --m_pos;
        return tmp;
    }

    grid2d_iterator& operator+=(size_type n)
    {
        m_pos += n;
        return *this;
    }

    grid2d_iterator& operator+=(const grid2d_iterator& i)
    {
        m_pos += i.m_pos;
        return *this;
    }

    grid2d_iterator& operator-=(size_type n)
    {
        m_pos -= n;
        return *this;
    }

    grid2d_iterator& operator-=(const grid2d_iterator& i)
    {
        m_pos -= i.m_pos;
        return *this;
    }

    template <class R, class Sx, class Sy>
    friend grid2d_iterator<R, Sx, Sy> operator+(const grid2d_iterator<R, Sx, Sy>&, typename grid2d_iterator<R, Sx, Sy>::size_type);
    template <class R, class Sx, class Sy>
    friend grid2d_iterator<R, Sx, Sy> operator+(typename grid2d_iterator<R, Sx, Sy>::size_type, const grid2d_iterator<R, Sx, Sy>&);
    template <class R, class Sx, class Sy>
    friend typename grid2d_iterator<R, Sx, Sy>::difference_type operator-(const grid2d_iterator<R, Sx, Sy>&, const grid2d_iterator<R, Sx, Sy>&);
    template <class R, class Sx, class Sy>
    friend grid2d_iterator<R, Sx, Sy> operator-(const grid2d_iterator<R, Sx, Sy>&, typename grid2d_iterator<R, Sx, Sy>::size_type);


    template <class R, class Sx, class Sy>
    friend bool operator!=(const grid2d_iterator<R, Sx, Sy>&, const grid2d_iterator<R, Sx, Sy>&);
    template <class R, class Sx, class Sy>
    friend bool operator==(const grid2d_iterator<R, Sx, Sy>&, const grid2d_iterator<R, Sx, Sy>&);
    template <class R, class Sx, class Sy>
    friend bool operator>(const grid2d_iterator<R, Sx, Sy>&, const grid2d_iterator<R, Sx, Sy>&);
    template <class R, class Sx, class Sy>
    friend bool operator>=(const grid2d_iterator<R, Sx, Sy>&, const grid2d_iterator<R, Sx, Sy>&);
    template <class R, class Sx, class Sy>
    friend bool operator<(const grid2d_iterator<R, Sx, Sy>&, const grid2d_iterator<R, Sx, Sy>&);
    template <class R, class Sx, class Sy>
    friend bool operator<=(const grid2d_iterator<R, Sx, Sy>&, const grid2d_iterator<R, Sx, Sy>&);
private:
    size_type m_pos;
    real_type m_xmin;
    real_type m_xmax;
    real_type m_ymin;
    real_type m_ymax;
    size_type m_size_x;
    size_type m_size_y;
};


template <class R, class Sx, class Sy>
inline grid2d_iterator<R, Sx, Sy> operator+(const grid2d_iterator<R, Sx, Sy>& it, typename grid2d_iterator<R, Sx, Sy>::size_type n)
{
    return grid2d_iterator<R, Sx, Sy>(it.m_pos + n, it.m_xmin, it.m_xmax, it.m_ymin, it.m_ymax, it.m_size_x, it.m_size_y);
}

template <class R, class Sx, class Sy>
inline grid2d_iterator<R, Sx, Sy> operator+(typename grid2d_iterator<R, Sx, Sy>::size_type n, const grid2d_iterator<R, Sx, Sy>& it)
{
    return (it + n);
}

template <class R, class Sx, class Sy>
inline typename grid2d_iterator<R, Sx, Sy>::difference_type operator-(const grid2d_iterator<R, Sx, Sy>& it1, const grid2d_iterator<R, Sx, Sy>& it2)
{
    return static_cast<typename grid2d_iterator<R, Sx, Sy>::difference_type>(it1.m_pos - it2.m_pos);
}

template <class R, class Sx, class Sy>
inline grid2d_iterator<R, Sx, Sy> operator-(const grid2d_iterator<R, Sx, Sy>& it, typename grid2d_iterator<R, Sx, Sy>::size_type n)
{
    return grid2d_iterator<R, Sx, Sy>(it.m_pos - n, it.m_xmin, it.m_xmax, it.m_ymin, it.m_ymax, it.m_size_x, it.m_size_y);
}

template <class R, class Sx, class Sy>
inline bool operator!=(const grid2d_iterator<R, Sx, Sy>& x, const grid2d_iterator<R, Sx, Sy>& y)
{
    return x.m_pos != y.m_pos;
}

template <class R, class Sx, class Sy>
inline bool operator==(const grid2d_iterator<R, Sx, Sy>& x, const grid2d_iterator<R, Sx, Sy>& y)
{
    return x.m_pos == y.m_pos;
}

template <class R, class Sx, class Sy>
inline bool operator>(const grid2d_iterator<R, Sx, Sy>& x, const grid2d_iterator<R, Sx, Sy>& y)
{
    return x.m_pos > y.m_pos;
}

template <class R, class Sx, class Sy>
inline bool operator>=(const grid2d_iterator<R, Sx, Sy>& x, const grid2d_iterator<R, Sx, Sy>& y)
{
    return x.m_pos >= y.m_pos;
}

template <class R, class Sx, class Sy>
inline bool operator<(const grid2d_iterator<R, Sx, Sy>& x, const grid2d_iterator<R, Sx, Sy>& y)
{
    return x.m_pos < y.m_pos;
}

template <class R, class Sx, class Sy>
inline bool operator<=(const grid2d_iterator<R, Sx, Sy>& x, const grid2d_iterator<R, Sx, Sy>& y)
{
    return x.m_pos <= y.m_pos;
}


namespace internal {

struct as_row{};
struct as_col{};

template <class T, class S = as_row>
class const_row_col_iterator
{
public:
    static_assert(std::is_same_v<S, as_row> || std::is_same_v<S, as_col>);

    typedef std::random_access_iterator_tag  iterator_category;
    typedef T  value_type;
    typedef ptrdiff_t difference_type;
    typedef size_t size_type;
    typedef T* pointer;
    typedef T& reference;
    typedef const T& const_reference;

    const_row_col_iterator(size_type i, size_type j, const std::vector<value_type> & data, size_type cols)
        : m_i{i}, m_j{j}, m_data{data}, m_cols{cols}
    {}

    const_row_col_iterator(const const_row_col_iterator &) = default;

    const_row_col_iterator& operator=(const const_row_col_iterator&) = delete;

    const_reference operator*() const
    {
        return m_data[m_i * m_cols + m_j];
    }

    const_row_col_iterator& operator++()
    {
        if constexpr (std::is_same_v<S, as_row>) { ++m_j; } else { ++m_i; }

        return *this;
    }

    const_row_col_iterator operator++(int)
    {
        const_row_col_iterator tmp(*this);

        if constexpr (std::is_same_v<S, as_row>) { ++m_j; } else { ++m_i; }

        return tmp;
    }

    const_row_col_iterator& operator--()
    {
        if constexpr (std::is_same_v<S, as_row>) { --m_j; } else { --m_i; }

        return *this;
    }

    const_row_col_iterator operator--(int)
    {
        const_row_col_iterator tmp(*this);

        if constexpr (std::is_same_v<S, as_row>) { --m_j; } else { --m_i; }

        return tmp;
    }

    const_row_col_iterator& operator+=(difference_type n)
    {
        if constexpr (std::is_same_v<S, as_row>) { m_j += n; } else { m_i += n; };

        return *this;
    }

    const_row_col_iterator& operator-=(difference_type n)
    {
        if constexpr (std::is_same_v<S, as_row>) { m_j -= n; } else { m_i -= n; };

        return *this;
    }

    const_row_col_iterator operator+(difference_type n) const
    {
        if constexpr (std::is_same_v<S, as_row>) {

            return const_row_col_iterator(m_i, m_j+n, m_data, m_cols);
        }

        return const_row_col_iterator(m_i+n, m_j, m_data, m_cols);
    }

    const_row_col_iterator operator-(difference_type n) const
    {
        if constexpr (std::is_same_v<S, as_row>) {
            return const_row_col_iterator(m_i, m_j-n, m_data, m_cols);
        }

        return const_row_col_iterator(m_i-n, m_j, m_data, m_cols);
    }

    difference_type operator-(const const_row_col_iterator & other) const
    {
        if constexpr (std::is_same_v<S, as_row>) {
            return m_j - other.m_j;
        }

        return m_i - other.m_i;
    }

    bool operator!=(const const_row_col_iterator & other) const
    {
        return (m_i != other.m_i) || (m_j != other.m_j) || (m_cols != other.m_cols);
    }

    bool operator==(const const_row_col_iterator & other) const
    {
        return (m_i == other.m_i) && (m_j == other.m_j) && (m_cols == other.m_cols);
    }

private:
    size_type m_i;
    size_type m_j;
    const std::vector<value_type> & m_data;
    size_type m_cols;
};


template <class T, class S = as_row>
class row_col_iterator
{
public:
    static_assert(std::is_same_v<S, as_row> || std::is_same_v<S, as_col>);

    typedef std::random_access_iterator_tag  iterator_category;
    typedef T  value_type;
    typedef ptrdiff_t difference_type;
    typedef size_t size_type;
    typedef T* pointer;
    typedef T& reference;
    typedef const T& const_reference;

    row_col_iterator(size_type i, size_type j, std::vector<value_type> & data, size_type cols)
        : m_i{i}, m_j{j}, m_data{data}, m_cols{cols}
    {}

    row_col_iterator(const row_col_iterator &) = default;

    row_col_iterator& operator=(const row_col_iterator&) = default;

    const_reference operator*() const
    {
        return m_data[m_i * m_cols + m_j];
    }

    reference operator*()
    {
        return m_data[m_i * m_cols + m_j];
    }

    row_col_iterator& operator++()
    {
        if constexpr (std::is_same_v<S, as_row>) { ++m_j; } else { ++m_i; }

        return *this;
    }

    row_col_iterator operator++(int)
    {
        row_col_iterator tmp(*this);

        if constexpr (std::is_same_v<S, as_row>) { ++m_j; } else { ++m_i; }

        return tmp;
    }

    row_col_iterator& operator--()
    {
        if constexpr (std::is_same_v<S, as_row>) { --m_j; } else { --m_i; }

        return *this;
    }

    row_col_iterator operator--(int)
    {
        row_col_iterator tmp(*this);

        if constexpr (std::is_same_v<S, as_row>) { --m_j; } else { --m_i; }

        return tmp;
    }

    row_col_iterator& operator+=(difference_type n)
    {
        if constexpr (std::is_same_v<S, as_row>) { m_j += n; } else { m_i += n; };

        return *this;
    }

    row_col_iterator& operator-=(difference_type n)
    {
        if constexpr (std::is_same_v<S, as_row>) { m_j -= n; } else { m_i -= n; };

        return *this;
    }

    row_col_iterator operator+(difference_type n) const
    {
        if constexpr (std::is_same_v<S, as_row>) {
            return row_col_iterator(m_i, m_j+n, m_data, m_cols);
        }

        return row_col_iterator(m_i+n, m_j, m_data, m_cols);
    }

    row_col_iterator operator-(difference_type n) const
    {
        if constexpr (std::is_same_v<S, as_row>) {
            return row_col_iterator(m_i, m_j-n, m_data, m_cols);
        }

        return row_col_iterator(m_i-n, m_j, m_data, m_cols);
    }

    difference_type operator-(const row_col_iterator & other) const
    {
        if constexpr (std::is_same_v<S, as_row>) {
            return m_j - other.m_j;
        }

        return m_i - other.m_i;
    }

    bool operator!=(const row_col_iterator & other) const
    {
        return (m_i != other.m_i) || (m_j != other.m_j) || (m_cols != other.m_cols);
    }

    bool operator==(const row_col_iterator & other) const
    {
        return (m_i == other.m_i) && (m_j == other.m_j) && (m_cols == other.m_cols);
    }

private:
    size_type m_i;
    size_type m_j;
    std::vector<value_type> & m_data;
    size_type m_cols;
};


} /* namespace internal */


template <class T, class U = T, class ScaleX = linear_scale, class ScaleY = linear_scale>
class grid2d
{
public:
    typedef size_t size_type;
    typedef T real_type;
    typedef U value_type;
    typedef point2d<real_type> point_type;
    typedef ScaleX x_scale_type;
    typedef ScaleY y_scale_type;
    typedef std::vector<value_type> container_type;
    typedef typename container_type::iterator iterator;
    typedef typename container_type::const_iterator const_iterator;
    typedef typename container_type::reference reference;
    typedef typename container_type::const_reference const_reference;
    typedef internal::const_row_col_iterator<U, internal::as_row> const_row_iterator;
    typedef internal::const_row_col_iterator<U, internal::as_col> const_col_iterator;
    typedef internal::row_col_iterator<U, internal::as_row> row_iterator;
    typedef internal::row_col_iterator<U, internal::as_col> col_iterator;

    grid2d() : m_xmin(0), m_xmax(0), m_ymin(0), m_ymax(0), m_size_x(0), m_size_y(0) {}

    grid2d(const real_type& xmin, const real_type& xmax,
           const real_type& ymin, const real_type& ymax,
           size_type Nx, size_type Ny)
        : m_xmin(xmin), m_xmax(xmax), m_ymin(ymin), m_ymax(ymax), m_size_x(Nx), m_size_y(Ny), m_grid(Nx*Ny) {}

    grid2d(const grid2d &) = default;

    grid2d(grid2d &&) = default;

    grid2d& operator=(const grid2d &) = default;

    grid2d& operator=(grid2d &&) = default;

    const_row_iterator begin_row(size_type i) const
    {
        return const_row_iterator(i, 0, m_grid, m_size_y);
    }

    const_row_iterator end_row(size_type i) const
    {
        return const_row_iterator(i, m_size_y, m_grid, m_size_y);
    }

    const_col_iterator begin_col(size_type j) const
    {
        return const_col_iterator(0, j, m_grid, m_size_y);
    }

    const_col_iterator end_col(size_type j) const
    {
        return const_col_iterator(m_size_x, j, m_grid, m_size_y);
    }

    row_iterator begin_row(size_type i)
    {
        return row_iterator(i, 0, m_grid, m_size_y);
    }

    row_iterator end_row(size_type i)
    {
        return row_iterator(i, m_size_y, m_grid, m_size_y);
    }

    col_iterator begin_col(size_type j)
    {
        return col_iterator(0, j, m_grid, m_size_y);
    }

    col_iterator end_col(size_type j)
    {
        return col_iterator(m_size_x, j, m_grid, m_size_y);
    }

    iterator begin() { return m_grid.begin(); }

    iterator end() { return m_grid.end(); }

    const_iterator begin() const { return m_grid.begin(); }

    const_iterator end() const { return m_grid.end(); }

    const_reference operator()(size_type i, size_type j) const { return m_grid[i * m_size_y + j]; }

    reference operator()(size_type i, size_type j) { return m_grid[i * m_size_y + j]; }

    grid2d_iterator<real_type, x_scale_type, y_scale_type> first_point() const
    {
        return grid2d_iterator<real_type, x_scale_type, y_scale_type>(0, m_xmin, m_xmax, m_ymin, m_ymax, m_size_x, m_size_y);
    }

    grid2d_iterator<real_type, x_scale_type, y_scale_type> last_point() const
    {
        return grid2d_iterator<real_type, x_scale_type, y_scale_type>(m_grid.size(), m_xmin, m_xmax, m_ymin, m_ymax, m_size_x, m_size_y);
    }

    size_type size() const { return m_grid.size(); }

    size_type xsize() const { return m_size_x; }

    size_type ysize() const { return m_size_y; }

    real_type xmin() const { return m_xmin; }

    real_type xmax() const { return m_xmax; }

    real_type ymin() const { return m_ymin; }

    real_type ymax() const { return m_ymax; }

    real_type x(size_type i) const { return x_scale_type::get(i, m_xmin, m_xmax, m_size_x); }

    real_type y(size_type j) const { return y_scale_type::get(j, m_ymin, m_ymax, m_size_y); }

    void set_xrange(const real_type& min, const real_type& max)
    {
        m_xmin = min;
        m_xmax = max;
    }

    void set_yrange(const real_type& min, const real_type& max)
    {
        m_ymin = min;
        m_ymax = max;
    }

    void resize(size_type Nx, size_type Ny)
    {
        m_size_x = Nx;
        m_size_y = Ny;
        m_grid.resize(Nx * Ny);
    }

    template <class F>
    void apply(F && f)
    {
        std::transform(first_point(), last_point(), m_grid.begin(), std::forward<F>(f));
    }

    template <class F>
    void parallel_apply(F && f)
    {
        static thread_local thread_pool th_pool;

        const size_t ntasks = std::min( th_pool.num_threads(), m_grid.size() );

        const size_t ncalls_per_task = m_grid.size() / ntasks;

        const size_t n = m_grid.size() % ntasks;

        auto task = [f = std::forward<F>(f), this](size_t i, size_t j)
        {
            auto it = grid2d_iterator<real_type, x_scale_type, y_scale_type>(i,
                                                                             m_xmin, m_xmax,
                                                                             m_ymin, m_ymax,
                                                                             m_size_x, m_size_y);

            for (; i < j; ++i, ++it)
                m_grid[i] = f(*it);
        };

        std::vector<std::future<void>> data(ntasks);
        size_t m{ 0 };
        for ( size_t i = 0; i < ntasks; ++i )
        {
            size_t block = ( i < n ) ? ncalls_per_task + 1 : ncalls_per_task;
            data[i] = th_pool.add_task(task, m, m + block);
            m += block;
        }

        for (auto& x : data)
            x.wait();
    }

    template <class F>
    void serialize(std::ostream& out, F f, const char* sep = "\t")
    {
        for (size_type i = 0; i < m_size_x; ++i)
        {
            for (size_type j = 0; j < m_size_y; ++j)
            {
                out << f(m_grid[i * m_size_y + j]);  (j < m_size_y - 1) ? out << sep : out << std::endl;
            }
        }
    }


    void serialize(std::ostream& out, const char* sep = "\t")
    {
        for (size_type i = 0; i < m_size_x; ++i)
        {
            for (size_type j = 0; j < m_size_y; ++j)
            {
                out << m_grid[i * m_size_y + j];  (j < m_size_y - 1) ? out << sep : out << std::endl;
            }
        }
    }

private:
    real_type m_xmin;
    real_type m_xmax;
    real_type m_ymin;
    real_type m_ymax;
    size_type m_size_x;
    size_type m_size_y;
    container_type m_grid;
};

} /* namespace specmath */

#endif // GRID2D_H
