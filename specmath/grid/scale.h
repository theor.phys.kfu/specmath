#ifndef SCALE_H
#define SCALE_H

#include <cmath>
#include <specmath/common_functions.h>

namespace specmath {

struct linear_scale
{
    template <class T>
    static T get(size_t i, const T& min, const T& max, size_t npoints)
    {
        return min + i * (max - min)/(npoints - 1);
    }
};

struct sqr_scale
{
    template <class T>
    static T get(size_t i, const T& min, const T& max, size_t npoints)
    {
        return min + (max-min)*sqr(static_cast<T>(i)/(npoints-1));
    }
};

struct hyper_scale
{
    template <class T>
    static T get(size_t i, const T& min, const T& max, size_t npoints)
    {
        const T a = (npoints*max - min)/(npoints-1);
        const T b = ((min - max)*npoints)/(npoints-1);

        return a + b/(i+1);
    }
};

struct log_scale
{
    template <class T>
    static T get(size_t i, const T& min, const T& max, size_t npoints)
    {
        static const T e = exp(T(1));
        T ln_min = log(min);
        T ln_max = log(max);
        T ln_xi = linear_scale::get(i, ln_min, ln_max, npoints);

        return pow(T(e), ln_xi);
    }
};


struct log10_scale
{
    template <class T>
    static T get(size_t i, const T& min, const T& max, size_t npoints)
    {
        T log10_min = log10(min);
        T log10_max = log10(max);
        T log10_xi = linear_scale::get(i, log10_min, log10_max, npoints);

        return pow(T(10), log10_xi);
    }
};

} /* namespace specmath */

#endif // SCALE_H
