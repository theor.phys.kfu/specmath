#ifndef GRID1D_H
#define GRID1D_H

#include <vector>
#include <iostream>
#include <cstddef>
#include <functional>
#include <algorithm>
#include <specmath/thread_pool.h>

#include "scale.h"

namespace specmath {

template <class T, class Scale = linear_scale>
class grid1d_iterator
{
public:
    typedef T  real_type;
    typedef T  value_type;
    typedef ptrdiff_t difference_type;
    typedef std::random_access_iterator_tag  iterator_category;
    typedef const T* pointer;
    typedef const T& reference;
    typedef size_t size_type;
    typedef Scale scale_type;

    grid1d_iterator() : m_pos(0), m_min(0), m_max(0), m_size(2) {}

    grid1d_iterator(size_type pos, const real_type& min, const real_type& max, size_type N)
        : m_pos(pos), m_min(min), m_max(max), m_size(N) {}

    grid1d_iterator(const grid1d_iterator& ) = default;

    value_type operator*() const { return scale_type::get(m_pos, m_min, m_max, m_size); }

    grid1d_iterator& operator=(const grid1d_iterator& ) = default;

    grid1d_iterator& operator++()
    {
        ++m_pos;
        return *this;
    }

    grid1d_iterator operator++(int)
    {
        grid1d_iterator tmp(*this);
        ++m_pos;
        return tmp;
    }

    grid1d_iterator& operator--()
    {
        --m_pos;
        return *this;
    }

    grid1d_iterator operator--(int)
    {
        grid1d_iterator tmp(*this);
        --m_pos;
        return tmp;
    }

    grid1d_iterator& operator+=(size_type n)
    {
        m_pos += n;
        return *this;
    }

    grid1d_iterator& operator+=(const grid1d_iterator& i)
    {
        m_pos += i.m_pos;
        return *this;
    }

    grid1d_iterator& operator-=(size_type n)
    {
        m_pos -= n;
        return *this;
    }

    grid1d_iterator& operator-=(const grid1d_iterator& i)
    {
        m_pos -= i.m_pos;
        return *this;
    }

    template <class R, class S>
    friend grid1d_iterator<R, S> operator+(const grid1d_iterator<R, S>&, typename grid1d_iterator<R, S>::size_type);
    template <class R, class S>
    friend grid1d_iterator<R, S> operator+(typename grid1d_iterator<R, S>::size_type, const grid1d_iterator<R, S>&);
    template <class R, class S>
    friend typename grid1d_iterator<R, S>::difference_type operator-(const grid1d_iterator<R, S>&, const grid1d_iterator<R, S>&);
    template <class R, class S>
    friend grid1d_iterator<R, S> operator-(const grid1d_iterator<R, S>&, typename grid1d_iterator<R, S>::size_type);


    template <class R, class S>
    friend bool operator!=(const grid1d_iterator<R, S>&, const grid1d_iterator<R, S>&);
    template <class R, class S>
    friend bool operator==(const grid1d_iterator<R, S>&, const grid1d_iterator<R, S>&);
    template <class R, class S>
    friend bool operator>(const grid1d_iterator<R, S>&, const grid1d_iterator<R, S>&);
    template <class R, class S>
    friend bool operator>=(const grid1d_iterator<R, S>&, const grid1d_iterator<R, S>&);
    template <class R, class S>
    friend bool operator<(const grid1d_iterator<R, S>&, const grid1d_iterator<R, S>&);
    template <class R, class S>
    friend bool operator<=(const grid1d_iterator<R, S>&, const grid1d_iterator<R, S>&);
private:
    size_type m_pos;
    real_type m_min;
    real_type m_max;
    size_type m_size;
};


template <class R, class S>
inline grid1d_iterator<R, S> operator+(const grid1d_iterator<R, S>& x, typename grid1d_iterator<R, S>::size_type n)
{
    return grid1d_iterator<R, S>(x.m_pos + n, x.m_min, x.m_max, x.m_size);
}

template <class R, class S>
inline grid1d_iterator<R, S> operator+(typename grid1d_iterator<R, S>::size_type n, const grid1d_iterator<R, S>& x)
{
    return grid1d_iterator<R, S>(x.m_pos + n, x.m_min, x.m_max, x.m_size);
}

template <class R, class S>
inline typename grid1d_iterator<R, S>::difference_type operator-(const grid1d_iterator<R, S>& x, const grid1d_iterator<R, S>& y)
{
    return static_cast<typename grid1d_iterator<R, S>::difference_type>(x.m_pos - y.m_pos);
}

template <class R, class S>
inline grid1d_iterator<R, S> operator-(const grid1d_iterator<R, S>& x, typename grid1d_iterator<R, S>::size_type n)
{
    return grid1d_iterator<R, S>(x.m_pos - n, x.m_min, x.m_max, x.m_size);
}

template <class R, class S>
inline bool operator!=(const grid1d_iterator<R, S>& x, const grid1d_iterator<R, S>& y)
{
    return x.m_pos != y.m_pos;
}

template <class R, class S>
inline bool operator==(const grid1d_iterator<R, S>& x, const grid1d_iterator<R, S>& y)
{
    return x.m_pos == y.m_pos;
}

template <class R, class S>
inline bool operator>(const grid1d_iterator<R, S>& x, const grid1d_iterator<R, S>& y)
{
    return x.m_pos > y.m_pos;
}

template <class R, class S>
inline bool operator>=(const grid1d_iterator<R, S>& x, const grid1d_iterator<R, S>& y)
{
    return x.m_pos >= y.m_pos;
}

template <class R, class S>
inline bool operator<(const grid1d_iterator<R, S>& x, const grid1d_iterator<R, S>& y)
{
    return x.m_pos < y.m_pos;
}

template <class R, class S>
inline bool operator<=(const grid1d_iterator<R, S>& x, const grid1d_iterator<R, S>& y)
{
    return x.m_pos <= y.m_pos;
}


template <class T, class U = T, class Scale = linear_scale>
class grid1d
{
public:
    typedef size_t size_type;
    typedef T real_type;
    typedef U value_type;
    typedef T point_type;
    typedef Scale scale_type;
    typedef std::vector<value_type> container_type;
    typedef typename container_type::iterator iterator;
    typedef typename container_type::const_iterator const_iterator;
    typedef typename container_type::reference reference;
    typedef typename container_type::const_reference const_reference;

    grid1d() : m_min(0), m_max(0) {}

    grid1d(const real_type& min, const real_type& max, size_t N)
        : m_min(min), m_max(max), m_grid(N) {}

    grid1d(const grid1d &) = default;

    grid1d(grid1d &&) = default;

    grid1d& operator=(const grid1d &) = default;

    grid1d& operator=(grid1d &&) = default;

    iterator begin() { return m_grid.begin(); }

    iterator end() { return m_grid.end(); }

    const_iterator begin() const { return m_grid.begin(); }

    const_iterator end() const { return m_grid.end(); }

    const_reference operator()(size_type i) const { return m_grid[i]; }

    reference operator()(size_type i) { return m_grid[i]; }

    grid1d_iterator<real_type, scale_type> first_point() const { return grid1d_iterator<real_type, scale_type>(0, m_min, m_max, m_grid.size()); }

    grid1d_iterator<real_type, scale_type> last_point() const { return grid1d_iterator<real_type, scale_type>(m_grid.size(), m_min, m_max, m_grid.size()); }

    size_type size() const { return m_grid.size(); }

    real_type min() const { return m_min; }

    real_type max() const { return m_max; }

    real_type x(size_type i) const { return scale_type::get(i, m_min, m_max, m_grid.size()); }

    void set_range(const real_type& min, const real_type& max)
    {
        m_min = min;
        m_max = max;
    }

    void resize(size_type n)
    {
        m_grid.resize(n);
    }

    template <class F>
    void apply(F && f)
    {
        std::transform(first_point(), last_point(), m_grid.begin(), std::forward<F>(f));
    }

    template <class F>
    void parallel_apply(F && f)
    {
        static thread_local thread_pool th_pool;

        const size_t ntasks = std::min( th_pool.num_threads(), m_grid.size() );

        const size_t ncalls_per_task = m_grid.size() / ntasks;

        const size_t n = m_grid.size() % ntasks;

        auto task = [f = std::forward<F>(f), this](size_t i, size_t j)
        {
            auto it = grid1d_iterator<real_type, scale_type>(i, m_min, m_max, m_grid.size());

            for (; i < j; ++i, ++it)
                m_grid[i] = f(*it);
        };

        std::vector<std::future<void>> data(ntasks);
        size_t m{ 0 };
        for ( size_t i = 0; i < ntasks; ++i )
        {
            size_t block = ( i < n ) ? ncalls_per_task + 1 : ncalls_per_task;
            data[i] = th_pool.add_task(task, m, m + block);
            m += block;
        }

        for (auto& x : data)
            x.wait();
    }

    template <class F>
    void serialize(std::ostream& out, F f, const char* sep = "\t")
    {
        auto it = first_point();
        for (const auto & val : m_grid)
        {
            out << *(it++) << sep << f(val) << std::endl;
        }
    }

    void serialize(std::ostream& out, const char* sep = "\t")
    {
        auto it = first_point();
        for (const auto & val : m_grid)
        {
            out << *(it++) << sep << val << std::endl;
        }
    }


private:
    real_type m_min;
    real_type m_max;
    container_type m_grid;
};

} /* namespace specmath */

#endif // GRID1D_H
