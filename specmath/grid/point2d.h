#ifndef POINT2D_H
#define POINT2D_H

#include <iostream>

namespace specmath {

template <class T>
struct point2d
{
    typedef T value_t;

    point2d() : x(0), y(0) {}

    template <class R>
    point2d(const R& _x, const R& _y)
        : x(_x), y(_y) {}


    template <class R>
    point2d(const point2d<R>& p)
        : x(p.x), y(p.y) {}


    template <class R>
    point2d& operator=(const point2d<R>& p)
    {
        x = p.x;
        y = p.y;
        return *this;
    }

    value_t x;
    value_t y;
};

template <class T>
const point2d<T> operator+(const point2d<T> & p1, const point2d<T> & p2)
{
    return point2d<T>(p1.x + p2.x, p1.y + p2.y);
}

template <class T>
const point2d<T> operator-(const point2d<T> & p1, const point2d<T> & p2)
{
    return point2d<T>(p1.x - p2.x, p1.y - p2.y);
}

template <class T, class U>
const point2d<T> operator*(const point2d<T> & p, const U & c)
{
    return point2d<T>(p.x*c, p.y*c);
}

template <class T, class U>
const point2d<T> operator*(const U & c, const point2d<T> & p)
{
    return point2d<T>(c*p.x, c*p.y);
}

} /* namespace specmath */

template <class T>
std::ostream& operator<<(std::ostream& os, const specmath::point2d<T> & p)
{
    os << '(' << p.x << ',' << p.y << ')';
    return os;
}

template <class T>
std::istream& operator>>(std::istream& is, specmath::point2d<T> & p)
{
    char c;
    is >> c >> p.x >> c >> p.y >> c;
    return is;
}


#endif // POINT2D_H
