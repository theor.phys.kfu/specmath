#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <type_traits>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <future>
#include <memory>
#include <functional>

namespace specmath {

class thread_pool
{
public:
    explicit thread_pool(size_t num_threads = std::max(size_t{1}, size_t{std::thread::hardware_concurrency()}))
        : m_stop(false)
    {
        m_threads.reserve(num_threads);

        for (size_t i = 0; i < num_threads; ++i)
            m_threads.emplace_back(&thread_pool::worker, this);
    }

    thread_pool(const thread_pool &) = delete;
    thread_pool(thread_pool &&) = delete;

    thread_pool & operator=(const thread_pool &) = delete;
    thread_pool & operator=(thread_pool &&) = delete;

    size_t num_threads() const { return m_threads.size(); }

    ~thread_pool()
    {
        m_stop = true;
        
        m_cond.notify_all();

        for (auto & th : m_threads)
        {
            if (th.joinable())
                th.join();
        }
    }

    template<class F, class ...Args >
    auto add_task(F&& f, Args&& ...args)->std::future<std::invoke_result_t<F, Args...>>
    {
        using R = std::invoke_result_t<F, Args...>;

        auto task_ptr = std::make_shared<std::packaged_task<R()>>
            ( std::bind(std::forward<F>(f), std::forward<Args>(args)...) );

        {
            std::lock_guard<std::mutex> locker(m_mutex);
            m_queue.emplace( [=]() { (*task_ptr)(); } );
        }

        m_cond.notify_one();

        return task_ptr->get_future();
    }

private:
    std::atomic_bool m_stop;
    std::vector<std::thread> m_threads;
    std::queue<std::function<void()>> m_queue;
    std::mutex m_mutex;
    std::condition_variable m_cond;

    void worker()
    {
        for(;;)
        {
            std::unique_lock<std::mutex> locker(m_mutex);
            m_cond.wait(locker, [&]()
            {
                return !m_queue.empty() || m_stop;
            });

            if (m_stop)
                return;

            std::function<void()> task( std::move(m_queue.front()) );
            m_queue.pop();

            locker.unlock();

            if (task)
                task();
        }
    }
};

} /* namespace specmath */

#endif // THREAD_POOL_H
