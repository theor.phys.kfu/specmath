#ifndef ODE_SOLVER_H
#define ODE_SOLVER_H

#include <cmath>
#include <cstddef>
#include <type_traits>

namespace specmath {

struct euler {};

struct euler2 {};

struct rk4 {};

template <class>
class ode_solver;


class empty_observer
{
public:
    template <class Vector, class Real>
    void operator()(const Vector &, const Real &) const {}
};



template <>
class ode_solver<euler>
{
public:
    static constexpr short order = 1;

    template <class System, class Vector, class Real, class Observer = empty_observer>
    static Vector integrate(System && f,
                            const Vector & x0,
                            const Real & t0,
                            const Real & t,
                            const Real & dt_max,
                            Observer && observer = empty_observer())
    {
        static_assert(std::is_floating_point_v<Real>,
                      "T must be floating point type");

        static_assert(std::is_same_v<typename Vector::value_type, Real>,
                      "V::value_type must be floating point type");

        size_t N = 2 + fabs((t - t0)/dt_max);

        Real dt = (t - t0)/(N-1);

        Vector x = x0;
        Vector k1 = x0;

        for (size_t i = 0; i < N-1; ++i)
        {
            Real ti = t0 + i*dt;

            observer(x, ti);

            f(x, k1, ti);

            for (size_t n = 0; n < x.size(); ++n)
            {
                x[n] += k1[n]*dt;
            }
        }

        observer(x, t);

        return x;
    }
};

template <>
class ode_solver<euler2>
{
public:
    static constexpr short order = 2;

    template <class System, class Vector, class Real, class Observer = empty_observer>
    static Vector integrate(System && f,
                            const Vector & x0,
                            const Real & t0,
                            const Real & t,
                            const Real & dt_max,
                            Observer && observer = empty_observer())
    {
        static_assert(std::is_floating_point_v<Real>,
                      "T must be floating point type");

        static_assert(std::is_same_v<typename Vector::value_type, Real>,
                      "V::value_type must be floating point type");

        size_t N = 2 + fabs((t - t0)/dt_max);

        Real dt = (t - t0)/(N-1);

        Vector x = x0;
        Vector k1 = x0;
        Vector k2 = x0;

        for (size_t i = 0; i < N-1; ++i)
        {
            Real ti = t0 + i*dt;

            observer(x, ti);

            f(x, k1, ti);

            Vector cx = x;

            for (size_t n = 0; n < x.size(); ++n)
            {
                cx[n] += k1[n]*dt;
            }

            f(cx, k2, ti + dt);

            for (size_t n = 0; n < x.size(); ++n)
            {
                x[n] += 0.5*(k1[n] + k2[n])*dt;
            }
        }

        observer(x, t);

        return x;
    }
};

template <>
class ode_solver<rk4>
{
public:
    static constexpr short order = 4;

    template <class System, class Vector, class Real, class Observer = empty_observer>
    static Vector integrate(System && f,
                            const Vector & x0,
                            const Real & t0,
                            const Real & t,
                            const Real & dt_max,
                            Observer && observer = empty_observer())
    {
        static_assert(std::is_floating_point_v<Real>,
                      "T must be floating point type");

        static_assert(std::is_same_v<typename Vector::value_type, Real>,
                      "V::value_type must be floating point type");

        size_t N = 2 + fabs((t - t0)/dt_max);

        Real dt = (t - t0)/(N-1);

        Vector x = x0;
        Vector k1 = x0;
        Vector k2 = x0;
        Vector k3 = x0;
        Vector k4 = x0;

        for (size_t i = 0; i < N-1; ++i)
        {
            Real ti = t0 + i*dt;

            observer(x, ti);

            f(x, k1, ti);

            Vector cx = x;

            for (size_t n = 0; n < x.size(); ++n)
            {
                cx[n] += 0.5*k1[n]*dt;
            }

            f(cx, k2, ti+0.5*dt);

            for (size_t n = 0; n < x.size(); ++n)
            {
                cx[n] = x[n] + 0.5*k2[n]*dt;
            }

            f(cx, k3, ti+0.5*dt);

            for (size_t n = 0; n < x.size(); ++n)
            {
                cx[n] = x[n] + k3[n]*dt;
            }

            f(cx, k4, ti+dt);

            for (size_t n = 0; n < x.size(); ++n)
            {
                x[n] += (k1[n] + 2*( k2[n] + k3[n] ) + k4[n] )*dt/6;
            }
        }

        observer(x, t);

        return x;
    }
};

} /* namespace specmath */

#endif // ODE_SOLVER_H
