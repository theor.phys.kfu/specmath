#ifndef MC_INTEGRAL_H
#define MC_INTEGRAL_H

#include <cmath>
#include <algorithm>
#include <utility>
#include <vector>
#include <random>
#include <specmath/thread_pool.h>
#include <specmath/common_functions.h>

namespace specmath {

namespace mc {

using vec_t = std::vector<double>;

using bounds_t = std::vector<std::pair<double, double>>;

class result_t
{
public:
    result_t()
        : m_mean{ 0.0 }, m_variance{ 0.0 }, m_sample_size{ 0 }
    {}

    result_t(double mean, double variance, size_t sample_size)
        : m_mean{ mean }, m_variance{ variance }, m_sample_size{ sample_size }
    {}

    double mean() const { return m_mean; }

    double variance() const { return m_variance; }

    /*
     * confidence interval for three-sigma rule
    */
    double confidence_interval() const
    {
        return 3.0*sqrt(m_variance/m_sample_size);
    }

    size_t sample_size() const { return m_sample_size; }

    result_t& operator+=(const result_t& other)
    {
        using specmath::sqr;
        const double p1 = static_cast<double>(m_sample_size)/(m_sample_size + other.m_sample_size);
        const double p2 = 1.0 - p1;

        m_variance = p1*m_variance + p2*other.m_variance + p1*p2*sqr(m_mean - other.m_mean);

        m_mean = p1*m_mean + p2*other.m_mean;

        m_sample_size += other.m_sample_size;

        return *this;
    }

    friend result_t operator+(const result_t& x, const result_t& y);

private:
    double m_mean;
    double m_variance;
    size_t m_sample_size;
};

inline result_t operator+(const result_t& x1, const result_t& x2)
{
    using specmath::sqr;
    const double p1 = static_cast<double>(x1.m_sample_size)/(x1.m_sample_size + x2.m_sample_size);
    const double p2 = 1.0 - p1;

    double mean = p1*x1.m_mean + p2*x2.m_mean;

    double var = p1*x1.m_variance + p2*x2.m_variance + p1*p2*sqr( x1.m_mean - x2.m_mean );

    size_t sample_size = x1.m_sample_size + x2.m_sample_size;

    return result_t( mean, var, sample_size );
}

template <class F>
result_t integrate(F&& f, const bounds_t& bounds, size_t ncalls)
{
    static thread_local std::random_device rd;
    static thread_local std::mt19937 gen(rd());

    double V{1};
    for (const auto & range : bounds)
        V *= (range.second - range.first);

    double mean{0};
    double M2{0};

    vec_t x(bounds.size());

    for (size_t n = 0; n < ncalls; ++n)
    {
        for (size_t j = 0; j < bounds.size(); ++j)
        {
            x[j] = std::uniform_real_distribution<double>(bounds[j].first, bounds[j].second)(gen);
        }

        double d = f(x) - mean;
        mean += d/(n+1);
        M2 += d*d*n/(n+1);
    }

    return result_t(V*mean, V*V*M2/ncalls, ncalls);
}

namespace concurrency {

template <class F>
result_t integrate(F&& f, const bounds_t& bounds, size_t ncalls_per_task, size_t ntasks)
{
    static thread_local thread_pool th_pool;

    std::vector<std::future<result_t>> data(ntasks);

    for (size_t i = 0; i < data.size(); ++i)
    {
        data[i] = th_pool.add_task([f = std::forward<F>(f), &bounds, ncalls_per_task]
        {
            return mc::integrate(f, bounds, ncalls_per_task);
        });
    }

    result_t result;

    for (auto& x : data)
    {
        result += x.get();
    }

    return result;
}

template <class F>
result_t integrate(F&& f, const bounds_t& bounds, size_t ncalls)
{
    static thread_local thread_pool th_pool;

    const size_t ntasks = std::min( th_pool.num_threads(), ncalls );

    const size_t ncalls_per_task = ncalls / ntasks;

    const size_t n = ncalls % ntasks;

    std::vector<std::future<result_t>> data(ntasks);

    for (size_t i = 0; i < ntasks; ++i)
    {
        ncalls = ( i < n ) ? ncalls_per_task + 1 : ncalls_per_task;

        data[i] = th_pool.add_task([f = std::forward<F>(f), &bounds, ncalls]
        {
            return mc::integrate(f, bounds, ncalls);
        });
    }

    result_t result;

    for (auto& x : data)
    {
        result += x.get();
    }

    return result;
}

} /* namespace concurrency */

} /* namespace mc */

} /* namespace specmath */

#endif // MC_INTEGRAL_H
