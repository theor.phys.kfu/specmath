#ifndef BISECTION_ROOT_SOLVER_H
#define BISECTION_ROOT_SOLVER_H

#include <cmath>
#include <specmath/common_functions.h>
#include <stdexcept>

namespace specmath {

namespace detail {

template <class T>
T linear_interpolate(const T & a, const T & b, const T & fa, const T & fb)
{

    static constexpr T eps = 0.1;

    T x = a - (b-a)*fa/(fb-fa);

    if (x < a+eps*(b-a))
        return a+eps*(b-a);

    if (x > b-eps*(b-a))
        return b-eps*(b-a);

    return x;
}

} /* namespace detail */


template <class F, class T>
T bisect(F && function, const T & min, const T & max, const T & err)
{
    T f_min = function(min);
    if (f_min == 0)
        return min;

    T f_max = function(max);
    if (f_max == 0)
        return max;


    if (sign(f_min) * sign(f_max) > 0)
        throw std::logic_error("No change of sign in bisect, either there is no root to find, or there are multiple roots in the interval");

    T  x_min = min;
    T  x_max = max;
    do {
        T x_mid = detail::linear_interpolate(x_min, x_max, f_min, f_max);
        T f_mid = function(x_mid);

        if (sign(f_min) * sign(f_mid) > 0) {
            x_min = x_mid;
            f_min = f_mid;
        } else {
            x_max = x_mid;
            f_max = f_mid;
        }

    } while (fabs(x_max-x_min) > fabs(err));

    return detail::linear_interpolate(x_min, x_max, f_min, f_max);
}

} /* namespace specmath */

#endif // BISECTION_ROOT_SOLVER_H
