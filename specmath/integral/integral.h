#ifndef INTEGRAL_H
#define INTEGRAL_H

#include <stdexcept>
#include <specmath/common_functions.h>

namespace specmath {

struct trap{};
struct simps{};
struct amv3{};
struct amv4{};

template <class>
struct alg_traits;

template <>
struct alg_traits<simps>
{
    static constexpr unsigned norm_2d = 9;
    static constexpr unsigned norm_1d = 3;

    static unsigned coeff(unsigned i, unsigned j, unsigned Nx, unsigned Ny)
    {
        return coeff(i, Nx)*coeff(j, Ny);
    }

    static unsigned coeff(unsigned i, unsigned N)
    {
        if ((0 == i) || (N-1 == i))
            return 1;

        return is_odd(i) ? 4 : 2;
    }

    static void check_N(unsigned N)
    {
        if ((N < 3) || is_even(N))
            throw std::invalid_argument("alg_traits::check_N: N must be odd and greater than 2");
    }
};


template <>
struct alg_traits<trap>
{
    static constexpr unsigned norm_2d = 4;
    static constexpr unsigned norm_1d = 2;

    static constexpr unsigned coeff(unsigned i, unsigned j, unsigned Nx, unsigned Ny)
    {
        return coeff(i, Nx)*coeff(j, Ny);
    }

    static constexpr unsigned coeff(unsigned i, unsigned N)
    {
        return ((0 == i) || (N-1 == i)) ? 1 : 2;
    }

    static void check_N(unsigned N)
    {
        if (N < 2)
            throw std::invalid_argument("alg_traits::check_N: N must be greater than 1");
    }
};

template <>
struct alg_traits<amv4>
{
    static constexpr unsigned norm_2d = sqr(15*3);
    static constexpr unsigned norm_1d = 15*3;

    static constexpr unsigned coeff(unsigned i, unsigned j, unsigned Nx, unsigned Ny)
    {
        return coeff(i, Nx)*coeff(j, Ny);
    }

    static constexpr unsigned coeff(unsigned i, unsigned N)
    {
        if ((0 == i) || (N-1 == i))
            return 14;

        return is_odd(i) ? 64 : ((0 == (i % 4)) ? 28 : 24);
    }

    static void check_N(unsigned N)
    {
        if ((N < 5) || is_even(N))
            throw std::invalid_argument("alg_traits::check_N: N must be odd (2*2*n+1) and greater than 4");
    }
};

template <>
struct alg_traits<amv3>
{
    static constexpr unsigned norm_2d = sqr(7*3);
    static constexpr unsigned norm_1d = 7*3;

    static constexpr unsigned coeff(unsigned i, unsigned j, unsigned Nx, unsigned Ny)
    {
        return coeff(i, Nx)*coeff(j, Ny);
    }

    static constexpr unsigned coeff(unsigned i, unsigned N)
    {
        if ((0 == i) || (N-1 == i))
            return 6;

        return is_odd(i) ? 32 : ((0 == (i % 4)) ? 12 : 8);
    }

    static void check_N(unsigned N)
    {
        if ((N < 5) || is_even(N))
            throw std::invalid_argument("alg_traits::check_N: N must be odd (2*2*n+1) and greater than 4");
    }
};

template <class Alg, class F, class T>
inline std::invoke_result_t<F, T> integrate(F && f, const T & xmin, const T & xmax, unsigned N)
{
    alg_traits<Alg>::check_N(N);

    const T dx = (xmax - xmin)/T(N-1);

    std::invoke_result_t<F, T> sum{0};

    for (unsigned i = 0; i < N; ++i)
    {
        T xi = xmin + i*dx;
        sum += static_cast<std::invoke_result_t<F, T>>(alg_traits<Alg>::coeff(i, N))*f(xi);
    }

    return dx*sum/static_cast<T>(alg_traits<Alg>::norm_1d);
}


template <class Alg, class F, class T>
inline std::invoke_result_t<F, T, T> integrate(F && f,
                                            const T & xmin, const T & xmax,
                                            const T & ymin, const T & ymax,
                                            unsigned Nx, unsigned Ny)
{
    alg_traits<Alg>::check_N(Nx);
    alg_traits<Alg>::check_N(Ny);

    const T dx = (xmax - xmin)/T(Nx-1);
    const T dy = (ymax - ymin)/T(Ny-1);

    std::invoke_result_t<F, T, T> sum{0};

    for (unsigned i = 0; i < Nx; ++i)
    {
        T xi = xmin + i*dx;

        for (unsigned j = 0; j < Ny; ++j)
        {
            T yj = ymin + j*dy;

            sum += static_cast<std::invoke_result_t<F, T, T>>(alg_traits<Alg>::coeff(i, j, Nx, Ny))*f(xi, yj);
        }
    }

    return dx*dy*sum/static_cast<T>(alg_traits<Alg>::norm_2d);
}


template <class Alg, class Lattice, class U>
inline std::invoke_result_t<Lattice, U> mean_lattice_function(Lattice && lattice, U N)
{
    using T = std::invoke_result_t<Lattice, U>;
    alg_traits<Alg>::check_N(N);

    T sum{0};

    for (U i = 0; i < N; ++i)
    {
        sum += alg_traits<Alg>::coeff(i, N)*lattice(i);
    }
    return sum/static_cast<T>((N-1)*alg_traits<Alg>::norm_1d);
}


template <class Alg, class Lattice, class U>
inline std::invoke_result_t<Lattice, U, U> mean_lattice_function(Lattice && lattice, U Nx, U Ny)
{
    using T = std::invoke_result_t<Lattice, U, U>;
    alg_traits<Alg>::check_N(Nx);
    alg_traits<Alg>::check_N(Ny);

    T sum{0};

    for (U i = 0; i < Nx; ++i)
    {
        for (U j = 0; j < Ny; ++j)
        {
            sum += alg_traits<Alg>::coeff(i, j, Nx, Ny)*lattice(i, j);
        }
    }

    return sum/static_cast<T>((Nx-1)*(Ny-1)*alg_traits<Alg>::norm_2d);
}


} /* namespace specmath */

#endif // INTEGRAL_H
