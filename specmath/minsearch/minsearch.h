#ifndef MINSEARCH_H
#define MINSEARCH_H

#include <algorithm>
#include <numeric>
#include <random>
#include <cmath>
#include <specmath/thread_pool.h>
#include <specmath/common_functions.h>

namespace specmath {

class empty_observer {
public:
    template <class V, class T>
    void operator()(const V & /* vector */,
                    const T & /* value */,
                    unsigned long /* steps */) const {}

};

template <class T>
class breaker
{
public:
    breaker(const T & min_r = 1e-6, unsigned long max_steps = 100000)
        : m_min_r{ min_r }, m_max_steps{ max_steps }
    {}

    bool operator()(const T & r, unsigned long steps) const
    {
        return (r <= m_min_r) || (steps >= m_max_steps);
    }

private:
    T m_min_r;
    unsigned long m_max_steps;
};


template <class T, class Rng = std::mt19937>
class minsearch
{
    typedef T real_type;
    typedef unsigned long uint_type;
    typedef Rng rng_type;

public:
    class params_type
    {
    public:
        params_type()
            : init_radius{ 1.0 }, point_per_step{ 10 }, alpha{ 0.98 }, beta{ 2.0 }
        {}

        params_type(const real_type & init_radius_,
                    uint_type point_per_step_,
                    const real_type & alpha_,
                    const real_type & beta_)
            : init_radius{ init_radius_ },
            point_per_step{ point_per_step_ },
            alpha{ alpha_ },
            beta{ beta_ }
        {}

        real_type init_radius;
        uint_type point_per_step;
        real_type alpha;
        real_type beta;
    };


    minsearch(const real_type & init_radius = 1.0, uint_type point_per_step = 10,
              const real_type & alpha = 0.98, const real_type & beta = 2.0)

        : m_params(init_radius, point_per_step, alpha, beta)
    {}

    minsearch(const params_type & params)
        : m_params{ params }
    {}

    void set_params(const params_type & params)
    {
        m_params = params;
    }

    params_type params() const
    {
        return m_params;
    }

    template <class Function, class Vector, class Breaker, class Observer = empty_observer>
    real_type find_minimum(Function && function, Vector & x, Breaker && _breaker, Observer && observer = empty_observer()) const
    {
        static_assert(std::is_same<real_type, typename Vector::value_type>::value,
                      "type mismatch: real_type != value_type");

        using vector_type = Vector;

        real_type min = function(x);
        vector_type dx = x;
        vector_type mean_x = x;

        const real_type mean_sqr_radius = x.size()/real_type(3);

        std::mutex mutex;

        auto task = [&](const real_type & r, std::atomic_int & ntests)
        {
            static thread_local std::random_device rd;
            static thread_local rng_type rng( rd() );

            while ( std::atomic_fetch_sub(&ntests, 1) > 0 )
            {
                vector_type cx = mean_x;
                std::transform(mean_x.begin(), mean_x.end(), cx.begin(), 
                [&](const real_type & x0)
                {
                    std::uniform_real_distribution<real_type> dist(x0-r, x0+r);

                    return dist( rng );
                });

                real_type cur_val = function(cx);

                std::lock_guard<std::mutex> locker(mutex);
                if (cur_val < min)
                {
                    x = std::move(cx);
                    min = cur_val;
                }
            }
        };

        static thread_local thread_pool pool;
        std::vector<std::future<void>> results( pool.num_threads() );

        real_type radius = m_params.init_radius;

        for( uint_type steps = 1; !_breaker(radius, steps); ++steps )
        {
            mean_x = x;
            std::atomic_int ntests( m_params.point_per_step );

            for (uint_type i = 0; i < results.size(); ++i)
            {
                results[i] = pool.add_task(task, radius, std::ref(ntests));
            }

            for (auto & res : results)
                res.wait();

            real_type sqr_ds{0};
            auto it_mean_x = mean_x.begin();
            for (auto it_x = x.begin(); it_x != x.end(); ++it_x, ++it_mean_x)
            {
                real_type dx = *it_x - *it_mean_x;
                sqr_ds += dx*dx;
            }

            if (sqr_ds < sqr(radius)*mean_sqr_radius)
                radius *= m_params.alpha;
            else
               radius = std::min(m_params.init_radius, radius*m_params.beta);

            observer(x, min, steps);
        }

        return min;
    }

private:
    params_type m_params;
};


} /* namespace specmath */

#endif // MINSEARCH_H
