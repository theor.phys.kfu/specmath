#ifndef MINSEARCH1D_H
#define MINSEARCH1D_H

#include <utility>
#include <cmath>

namespace specmath {

class minsearch1d
{
public:
    class param_t
    {
    public:
        param_t()
            : m_init_radius(1.0), m_alpha(0.8)
        {}

        explicit
        param_t(const double & init_radius, const double & alpha = 0.8)
            : m_init_radius(init_radius), m_alpha(alpha)
        {}

        double init_radius() const { return m_init_radius; }

        double alpha() const { return m_alpha; }

    private:
        double m_init_radius;
        double m_alpha;
    };

    explicit
    minsearch1d(const double & init_radius, const double & alpha = 0.8)
        : m_param(init_radius, alpha)
    {}

    explicit
    minsearch1d(const param_t & param)
        : m_param(param)
    {}

    param_t param() const { return m_param; }

    void param(const param_t & params)
    {
        m_param = params;
    }

    double init_radius() const { return m_param.init_radius(); }

    double alpha() const { return m_param.alpha(); }

    template <class F>
    double operator()(F && f, double & x, const double & err) const
    {
        int direction = 1;
        double f_min = f(x);
        double r = m_param.init_radius();

        while (r > err)
        {
            f_min = travel_to(std::forward<F>(f), f_min, x, r, direction);
            r *= m_param.alpha();
            direction *= -1;
        }
        return f_min;
    }

private:
    param_t m_param;

    template <class F>
    static double travel_to(F && f, double f_min, double & x, const double & r, int direction)
    {
        while (true)
        {
            double xi = x + direction*r;
            double fi = f(xi);
            if (fi < f_min)
            {
                f_min = fi;
                x = xi;
                continue;
            }
            break;
        }
        return f_min;
    }
};

} /* namespace specmath */

#endif // MINSEARCH1D_H
