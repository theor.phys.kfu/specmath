#ifndef MAP_PARSER_H
#define MAP_PARSER_H

#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <unordered_map>
#include <vector>
#include <regex>


namespace specmath {

class map_parser
{
public:
    map_parser()
        : m_file_is_loaded{ false }
    {}

    map_parser(const std::string & filename, char sc = '#')
    {
        load(filename, sc);
    }

    void load(const std::string & filename, char sc = '#')
    {
        std::ifstream in(filename.c_str());

        if ( m_file_is_loaded = in.is_open(); m_file_is_loaded )
        {
            std::ostringstream oss;

            oss << in.rdbuf();

            parse(oss.str(), sc);
        }
    }

    void clear() { m_map.clear(); }

    bool is_loaded() const { return m_file_is_loaded; }

    bool empty() const { return m_map.empty(); }

    size_t size() const { return m_map.size(); }

    std::vector<std::string> keys() const 
    {
        std::vector<std::string> _keys;
        _keys.reserve(m_map.size());

        for (const auto & p : m_map)
        {
            _keys.push_back(p.first);
        }

        return _keys;
    }

    bool contains(const std::string & key) const 
    {
        return m_map.find(key) != m_map.end();
    }

    template <class T>
    T get(const std::string & key, const T & default_val = T{}) const
    {
        if (auto it = m_map.find(key); it != m_map.end())
        {
            std::stringstream ss;
            ss << it->second;
            T res;
            return ( ss >> res ) ? res : default_val;
        }

        return default_val;
    }


private:
    bool m_file_is_loaded;
    std::unordered_map<std::string, std::string> m_map;

    template <class It>
    void try_parse(It first, It last)
    {
        if (first == last)
            return;

        static const std::regex r("(\\w+)\\s*=\\s*([\\w.+-]+)");
        std::regex_iterator<It> it(first, last, r);
        std::regex_iterator<It> end;

        for (; it != end; ++it)
        {
            m_map.try_emplace(it->str(1), it->str(2));
        }
    }

    void parse(const std::string & str, char sc)
    {
        auto first = str.begin();
        auto last = str.end();

        while ( first != last )
        {
            auto it = std::find(first, last, sc);

            if ( it != last )
            {
                try_parse(first, it);
                
                if ((first = std::find(++it, last, '\n')) != last)
                {
                    ++first;
                }
            }
            else
            {
                try_parse(first, it);
                break;
            }
        }
    }
};

} /* namespace specmath */

#endif // MAP_PARSER_H
