#ifndef KD_TREE_H
#define KD_TREE_H 

#include <algorithm>
#include <vector>
#include <utility>
#include <limits>
#include <cmath>
#include <stdexcept>

namespace specmath {

namespace compsci {
    
template <class Vector, class Data>
class kd_tree
{
public:
    typedef Vector vec_t;
    typedef Data data_t;
    typedef double real_t;
    typedef typename Vector::value_type coord_t;

    class result
    {
    public:
        result() = default;
        result(const result &) = default;

        result(const vec_t & point_, const data_t & data_, const real_t & distance_)
            : point(point_), data(data_), distance(distance_)
        {}

        result(result && other)
            : point(std::move(other.point)), data(std::move(other.data)),
              distance(other.distance)
        {}

        result & operator=(const result &) = default;

        result & operator=(result && other)
        {
            point = std::move(other.point);
            data = std::move(other.data);
            distance = other.distance;
            return *this;
        }

        vec_t point;
        data_t data;
        real_t distance = 0.0;
    };

    kd_tree(const kd_tree &) = delete;
    kd_tree& operator=(const kd_tree &) = delete;

    template<class ForwardIt>
    kd_tree(ForwardIt begin, ForwardIt end)
        : m_nodes(begin, end)
    {
        if (begin == end)
            return;

        m_dimensions = begin->first.size();
        m_root = make_tree(m_nodes.begin(), m_nodes.end(), 0);
    }

    bool empty() const { return m_nodes.empty(); }

    size_t dimensions() const { return m_dimensions; }

    size_t num_nodes() const { return m_nodes.size(); }

    result nearest(const vec_t & point) const
    {
        if (m_root == nullptr)
            throw std::logic_error("tree is empty!");

        if (point.size() != m_dimensions)
            throw std::logic_error("invalid input point size!");

        real_t best_dist = 0.0;
        node* best = nearest(m_root, point, 0, nullptr, best_dist);

        return result(best->m_point, best->m_data, std::sqrt(best_dist));
    }

private:
    struct node
    {
        node(const std::pair<vec_t, data_t> & pair)
            :   m_point(pair.first), m_data(pair.second),
                m_left(nullptr), m_right(nullptr)
        {}

        const coord_t & get(size_t index) const
        {
            return m_point[index];
        }

        real_t distance(const vec_t & pt) const
        {
            real_t sqr_d = 0.0;
            for (size_t i = 0; i < pt.size(); ++i)
            {
                real_t d = m_point[i] - pt[i];
                sqr_d += d*d;
            }
            return sqr_d;
        }

        vec_t m_point;
        data_t m_data;
        node* m_left;
        node* m_right;
    };

    size_t m_dimensions = 0;
    node* m_root = nullptr;
    std::vector<node> m_nodes;

    template <class RandomIt>
    node* make_tree(RandomIt begin, RandomIt end, size_t index)
    {
        if (begin == end)
            return nullptr;

        std::sort(begin, end, [&](const node & n1, const node & n2)
        {
            return n1.get(index) < n2.get(index);
        });

        auto middle = std::next(begin, std::distance(begin, end)/2);

        index = (index + 1) % m_dimensions;

        middle->m_left = make_tree(begin, middle, index);

        middle->m_right = make_tree(std::next(middle, 1), end, index);

        return std::addressof(*middle);
    }

    node* nearest(node* root, const vec_t & point,
                      size_t index, node* best, real_t & best_dist) const
    {
        if (root == nullptr)
            return best;

        real_t d = root->distance(point);

        if (best == nullptr || d < best_dist)
        {
            best_dist = d;
            best = root;
        }

        if (best_dist <= std::numeric_limits<real_t>::epsilon())
            return best;

        real_t dx = root->get(index) - point[index];

        index = (index + 1) % m_dimensions;

        best = nearest(dx > 0 ? root->m_left : root->m_right, point, index, best, best_dist);

        if (dx * dx >= best_dist)
            return best;

        return nearest(dx > 0 ? root->m_right : root->m_left, point, index, best, best_dist);
    }
};    
        
} /* namespace compsci */

} /* namespace specmath */


#endif // KD_TREE_H 
