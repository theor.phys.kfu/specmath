#ifndef WANDA_H
#define WANDA_H

#include <random>
#include <cmath>
#include <algorithm>
#include <limits>

#include "util.h"
#include "kd_tree.h"

namespace specmath {

namespace compsci {

class wanda
{
public:
    template <class ForwardIt>
    wanda(ForwardIt first, ForwardIt last)
        : m_kdtree(first, last), m_gen(std::random_device()())
    {}

    template <class Container>
    wanda(Container && c)
        : m_kdtree(c.begin(), c.end()), m_gen(std::random_device()())
    {}

    vec_t predict(const vec_t & data, size_t tests = 10) const
    {
        if (m_kdtree.empty())
            return vec_t();

        size_t N = std::min(m_kdtree.num_nodes(), (tests) ? tests : size_t(1));

        auto res = m_kdtree.nearest(data);

        if (res.distance < 10*std::numeric_limits<real_t>::epsilon())
            return res.data;

        vec_t output = res.data;

        const real_t r = sqrt(3.0/m_kdtree.dimensions()) * res.distance;

        std::uniform_real_distribution<real_t> dist(-r, r);

        for (size_t i = 1; i < N; ++i)
        {
            vec_t p = data;
            for (auto & x : p)
                x += dist(m_gen);

            auto stat = m_kdtree.nearest(p).data;

            for (size_t j = 0; j < output.size(); ++j)
            {
                output[j] += (stat[j] - output[j])/(i+1);
            }
        }

        return output;
    }

private:
    mutable kd_tree<vec_t, vec_t> m_kdtree;
    mutable std::mt19937 m_gen;
};


} /* namespace compsci */

} /* namespace specmath */

#endif // WANDA_H
