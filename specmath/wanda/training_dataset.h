#ifndef TRAINING_DATASET_H
#define TRAINING_DATASET_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <utility>
#include <stdexcept>
#include "util.h"

namespace specmath {

namespace compsci {

class training_dataset
{
public:
    typedef std::pair<vec_t, vec_t> training_data_t;
    typedef std::vector<training_data_t>::const_reference const_reference;
    typedef std::vector<training_data_t>::const_iterator const_iterator;

    training_dataset() = default;

    training_dataset(const training_dataset &) = default;

    training_dataset(training_dataset && other)
        : m_training_dataset(std::move(other.m_training_dataset))
    {}

    training_dataset & operator=(const training_dataset &) = default;

    training_dataset & operator=(training_dataset && other)
    {
        m_training_dataset = std::move(other.m_training_dataset);

        return *this;
    }

    void reserve(size_t size) { m_training_dataset.reserve(size); }

    void add(const vec_t & input_data, const vec_t & output_data)
    {
        if (input_data.empty())
            throw std::logic_error("input vector is empty!");

        if (output_data.empty())
            throw std::logic_error("output vector is empty!");

        if (!m_training_dataset.empty())
        {
            if (m_training_dataset[0].first.size() != input_data.size())
                throw std::logic_error("invalid input vector size!");

            if (m_training_dataset[0].second.size() != output_data.size())
                throw std::logic_error("invalid output vector size!");
        }

        m_training_dataset.emplace_back( input_data, output_data );
    }

    void clear() { m_training_dataset.clear(); }

    void serialize(std::ostream & os) const
    {
        if (m_training_dataset.empty())
            return;

        os << m_training_dataset.size() << std::endl;
        os << m_training_dataset[0].first.size() << std::endl;
        os << m_training_dataset[0].second.size() << std::endl;

        for (const auto & data : m_training_dataset)
        {
            for (const auto & x : data.first)
                os << x << " ";

            os << std::endl;

            for (const auto & x : data.second)
                os << x << " ";

            os << std::endl;
        }
    }
    
    void deserialize(std::istream & is)
    {
        m_training_dataset.clear();

        size_t size;
        size_t isize;
        size_t osize;

        is >> size >> isize >> osize;

        m_training_dataset.resize(size);

        for (auto & dataset : m_training_dataset)
        {
            dataset.first.resize(isize);
            dataset.second.resize(osize);

            for (auto & x : dataset.first)
                is >> x;

            for (auto & x : dataset.second)
                is >> x;
        }
    }

    size_t size() const { return m_training_dataset.size(); }

    size_t input_size() const
    {
        return m_training_dataset.empty() ? 0 : m_training_dataset[0].first.size();
    }

    size_t output_size() const
    {
        return m_training_dataset.empty() ? 0 : m_training_dataset[0].second.size();
    }

    bool empty() const { return m_training_dataset.empty(); }

    const_reference operator[](size_t i) const { return m_training_dataset[i]; }

    const_iterator begin() const { return m_training_dataset.begin(); }

    const_iterator end() const { return m_training_dataset.end(); }

    const_iterator erase(const_iterator pos) { return m_training_dataset.erase(pos); }

    const_iterator erase(const_iterator first, const_iterator last)
    {
        return m_training_dataset.erase(first, last);
    }

    template <class Gen>
    void shuffle(Gen && gen)
    {
        std::shuffle(m_training_dataset.begin(), m_training_dataset.end(), gen);
    }

private:
    std::vector<training_data_t> m_training_dataset;
};

} /* namespace compsci */

} /* namespace specmath */

#endif // TRAINING_DATASET_H
