#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <iterator>
#include <algorithm>
#include <utility>

namespace specmath {

namespace compsci {

typedef double real_t;

typedef std::vector<real_t> vec_t;

inline constexpr real_t sqr(const real_t & x)
{
    return x*x;
}

} /* namespace compsci */

} /* namespace specmath */

#endif // UTIL_H
